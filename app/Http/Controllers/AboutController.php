<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function view(){
        return view('about');
    }

    public function view2(){
        $data = "String Qualquer";
        return view('about2')->with('var_data',$data);
    }
}